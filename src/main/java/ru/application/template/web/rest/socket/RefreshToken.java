package ru.application.template.web.rest.socket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import ru.application.template.service.dto.StartRefresh;

@Controller
public class RefreshToken {
    private final SimpMessagingTemplate template;

    @Autowired
    RefreshToken(SimpMessagingTemplate template) {
        this.template = template;
    }

    @MessageMapping("/send/message")
    public void sendMessage(StartRefresh message) {
        System.out.println(message);
        this.template.convertAndSend("/message", message);
    }
}

package ru.application.template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationTemplateBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationTemplateBackendApplication.class, args);
    }

}

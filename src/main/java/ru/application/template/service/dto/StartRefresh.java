package ru.application.template.service.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Запрос на рефреш токен
 */
@Data
@ToString
@EqualsAndHashCode
public class StartRefresh {
    String command;
    String login;
}

package ru.application.template.service.dto;


import ru.application.template.domain.User;
import org.springframework.data.jpa.domain.Specification;

import java.io.Serializable;
import java.util.Objects;

public class UserFilter implements Serializable {
    private String login;
    private String firstName;

    public Specification<User> buildCriteria() {
        Specification<User> criteria = null;
        if (this.login != null) {
            criteria = (user, cq, cb) -> cb.equal(user.get("login"), this.login);
        }
        if (this.firstName != null) {
            if (criteria == null) {
                criteria = (user, cq, cb) -> cb.equal(user.get("firstName"), this.firstName);
            } else {
                criteria = criteria.and((user, cq, cb) -> cb.equal(user.get("firstName"), this.firstName));
            }
        }
        return criteria;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
       UserFilter that = (UserFilter) o;
        return Objects.equals(login, that.login) &&
                Objects.equals(firstName, that.firstName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, firstName);
    }

    @Override
    public String toString() {
        return "UserFilter{" +
                "login='" + login + '\'' +
                ", firstName='" + firstName + '\'' +
                '}';
    }
}

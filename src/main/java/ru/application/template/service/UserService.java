package ru.application.template.service;

import ru.application.template.domain.User;
import ru.application.template.service.dto.UserDTO;
import ru.application.template.service.dto.UserFilter;
import ru.application.template.repository.UserRepository;
import ru.application.template.service.mapper.UserMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PrepareUserService prepareUserService;

    UserService(UserRepository userRepository,
                UserMapper userMapper,
                PrepareUserService prepareUserService) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.prepareUserService = prepareUserService;
    }

    /**
     * получить всех пользаков c пагинацией  (гадо пдумать как реализовать)
     *
     * @param pageable пагинация
     * @return {@link UserDTO}
     */
    public Page<UserDTO> findAll(UserFilter criteria, Pageable pageable) {
        return this.userRepository.findAll(criteria.buildCriteria(), pageable).map(this.userMapper::userToUserDTO);
    }

    /**
     * создание пользователя через админку
     *
     * @param userDTO {@link UserDTO}
     * @return {@link UserDTO}
     */
    public UserDTO create(UserDTO userDTO) {
        return this.prepareUserService.prepareUserSave(userDTO, () -> {
                    User us = this.prepareUserService.encode(userDTO);
                    User res = this.userRepository.save(us);
                    return this.userMapper.userToUserDTO(res);
                }
        );
    }
    /**
     * обновление данных текущего пользователя
     *
     * @param userDTO {@link UserDTO}
     * @return {@link UserDTO}
     */
    public UserDTO updateUser(UserDTO userDTO) {
        return this.userMapper.userToUserDTO(this.userRepository.save(
                this.prepareUserService.prepareUserUpdate(userDTO)));
    }
    /**
     * получить пользователя по логину
     *
     * @param login логин пользователя
     * @return {@link UserDTO}
     */
    public UserDTO getUserByLogin(String login) {
        return this.userMapper.userToUserDTO(this.userRepository.findByLogin(login).orElse(new User()));
    }



}

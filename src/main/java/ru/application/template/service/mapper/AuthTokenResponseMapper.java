package ru.application.template.service.mapper;


import org.springframework.stereotype.Service;
import ru.application.template.domain.Role;
import ru.application.template.domain.User;
import ru.application.template.service.dto.AuthTokenResponseDTO;

import java.util.stream.Collectors;

@Service
public class AuthTokenResponseMapper {

    public AuthTokenResponseDTO toDTO(String jwtToken, User users) {
        AuthTokenResponseDTO authTokenResponseDTO = new AuthTokenResponseDTO();
        authTokenResponseDTO.setJwtToken(jwtToken);
        authTokenResponseDTO.setRoles(users.getRoles().stream().map(Role::getName).collect(Collectors.toList()));
        authTokenResponseDTO.setUsername(users.getLogin());
        return authTokenResponseDTO;
    }
}

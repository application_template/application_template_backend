package ru.application.template.service;

import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;
import ru.application.template.service.dto.StartRefresh;

@Service
public class RefreshService {
    private final SimpMessageSendingOperations messagingTemplate;

    public RefreshService(SimpMessageSendingOperations messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    public void sendMessage(String login) {
        StartRefresh startRefresh = new StartRefresh();
        startRefresh.setCommand("refresh");
        startRefresh.setLogin(login);
        this.messagingTemplate.convertAndSend("/message", startRefresh);
    }
}


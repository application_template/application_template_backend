package ru.application.template.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.application.template.domain.User;
import ru.application.template.service.dto.UserDTO;
import ru.application.template.repository.UserRepository;
import ru.application.template.service.mapper.UserMapper;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.function.Supplier;

/**
 * сервис для рефакторинга и подготовки пользователя для сохранения
 */
@Service
public class PrepareUserService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final UserMapper userMapper;
    private static final Logger LOGGER = LoggerFactory.getLogger(PrepareUserService.class);

    public PrepareUserService(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userMapper = userMapper;
    }

    /**
     * сохранение  пользователя в бд
     *
     * @param save функция сохранения которую надо реализовать
     * @return {@link UserDTO}
     */
    protected UserDTO prepareUserSave(UserDTO userDTO, Supplier<UserDTO> save) {
        if (userDTO.getLogin().equals(this.findUserByLogin(userDTO.getLogin()).getLogin())) {
            return userDTO.setErrorMessage("Пользователь с таким логином уже есть в бд");
        } else {
            return save.get();
        }
    }

    /**
     * подготовка пользователя к обновлению (проверка паролей)
     *
     * @param userDTO {@link UserDTO}
     * @return {@link User}
     */
    protected User prepareUserUpdate(UserDTO userDTO) {
        if (userDTO.getPwd() != null && !userDTO.getPwd().equals("")) {
            return this.encode(userDTO);
        } else {
            userDTO.setPwd(this.findUserByLogin(userDTO.getLogin()).getPwd());
            return this.userMapper.userDTOToUser(userDTO);
        }

    }

    /**
     * @param userDTO {@link UserDTO}
     * @return {@link User}
     */
    protected User encode(UserDTO userDTO) {
        if (userDTO.getPwd() != null) {
            userDTO.setPwd(this.bCryptPasswordEncoder.encode(userDTO.getPwd()));
        }
        LOGGER.info(userDTO.toString());
        return this.userMapper.userDTOToUser(userDTO);
    }

    /**
     * получить пользователя по логину
     *
     * @param login логин пользователя
     * @return {@link User}
     */
    private User findUserByLogin(String login) {
        return this.userRepository.findByLogin(login).orElse(new User());
    }

}

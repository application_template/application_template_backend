package ru.application.template.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * аспект для логирования сервисов
 */
@Aspect
@Component
public class AspectLogger {
    private static final Logger LOGGER = LoggerFactory.getLogger(AspectLogger.class);

    @Pointcut("execution(* ru.application.template.service.*.*(..))")
    private void allLogService() {
    }

    @Pointcut("execution(* ru.application.template.web.rest.*.*(..))")
    private void allLogRest() {
    }

    @Before(value = "allLogService() || allLogRest()")
    private void beforeLog(JoinPoint joinPoint) {
        LOGGER.debug("Выполнился метод:" + joinPoint.getTarget().getClass().getSimpleName() + " "
                + joinPoint.getSignature().getName());
        String args = Arrays.stream(joinPoint.getArgs()).filter(Objects::nonNull)
                .map(Object::toString)
                .collect(Collectors.joining(","));
        LOGGER.debug("Входящие параметры: " + joinPoint.toString() + ", args=[" + args + "]");
    }

    @AfterReturning(pointcut = "allLogService() || allLogRest()", returning = "ret")
    private void afterLog(Object ret) {
        LOGGER.debug("Возвращаемое значение: " + ret);
    }

}

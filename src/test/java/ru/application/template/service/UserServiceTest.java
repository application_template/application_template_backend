package ru.application.template.service;

import org.hamcrest.core.Is;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import ru.application.template.domain.Role;
import ru.application.template.domain.User;
import ru.application.template.repository.RoleRepository;
import ru.application.template.repository.UserRepository;
import ru.application.template.service.UserService;
import ru.application.template.service.dto.UserDTO;
import ru.application.template.service.mapper.UserMapper;

import java.util.Collections;
import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;

@DisplayName("тестирование: UsersService")
@TestPropertySource(locations = "classpath:application-h2.properties")
@SpringBootTest
class UserServiceTest {
    @Autowired
    UserService userService;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserMapper userMapper;

    @Test
    @DisplayName("тестирование: генерация логина")
    public void givenUsingJava8_whenGeneratingRandomAlphabeticString_thenCorrect() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 5;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        System.out.println(generatedString);
    }

    @Test
    @DisplayName("тестирование: генерация паролей")
    public void givenUsingJava8_whenGeneratingRandomAlphanumericString_thenCorrect() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        System.out.println(generatedString);
    }

    @Test
    @DisplayName("тестирование: добавление пользователя админом")
    public void createNeUserAdmin() {
        Role role = new Role(2L, "USER");
        role = this.roleRepository.save(role);
        UserDTO userDTO = new UserDTO();
        userDTO.setLogin("ADADASDSAD");
        userDTO.setPwd("kjsfksdfkjdshf");
        userDTO.setRoles(Collections.singletonList(role));
        assertThat(userService.create(userDTO).getId() != null,
                Is.is(true));
    }

    @Test
    @DisplayName("тестирование: обновление пользователя")
    public void testUpdateUser() {
        Role role = new Role(2L, "USER");
        role = this.roleRepository.save(role);
        UserDTO userDTO = new UserDTO();
        userDTO.setLogin("qwedwq545dqw");
        userDTO.setPwd("qdwqdwq");
        userDTO.setRoles(Collections.singletonList(role));
        UserDTO res = this.userService.create(userDTO);
        res.setPwd(null);
        res.setLastName("Вася");
        assertThat(this.userService.updateUser(res).getLastName(), Is.is("Вася"));
        assertThat(this.userRepository.findById(res.getId()).orElse(new User()).getLastName(), Is.is("Вася"));
    }


}

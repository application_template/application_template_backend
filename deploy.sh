# 1 аргумент ip сервера, 2 аргумент - имя приложения, 3 аргумент маска сборки (jar или war)
# ./deploy.sh 10.201.2.2 soul_template jar (пример вызова)
echo "$1"
echo "$2"
echo "$3"
ssh root@"$1" << EOF
cd /opt/apps/"$2"
./sh stop
mv *.$3 old
EOF
git checkout master
git pull
mvn clean
mvn package
scp target/*.$3 root@"$1":/opt/apps/"$2"
ssh root@"$1" << EOF
cd /opt/apps/"$2"
./sh start
EOF
